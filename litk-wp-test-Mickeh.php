<?php
/*
Plugin Name: litk-wp-test-MickeH
Description: Hello
Author: Michael Hansson
Version: 1.0
*/

// Creating the widget 
class My_Widget extends WP_Widget {

    public function __construct() {
        $widget_ops = array( 
            'classname' => 'my_widget',
            'description' => 'Test',
    );
        parent::__construct( 'my_widget', 'My Widget', $widget_ops );
        
    }

    public function widget( $args, $instance ) {

        global $wp_query;
        $id = $wp_query->get_queried_object_id();
        
        $args = array(
            'child_of'     => $id,
            'title_li'     => '',
            'depth'			=> 0,
            'sort_order'	=> 'DESC',
        );

        $pages = get_pages( $args );
        echo "<a id='drop-down-btn' class='drop-down' onclick='dropDownEvent()'>Child pages list</a>";
        echo "<div id='child-page-list' class='child-page-list'>";
        $output = '';
        
        foreach($pages as $value){
            $thumb = get_the_post_thumbnail( $value->ID, array(100,100), $attr = '' );
           
            $output .= "<li>";
            $output .= "<a href='" . $value->post_name . "' >" . $thumb . "</a><br />";
            $output .= "<a href='" . $value->post_name . "' >" .  $value->post_title . "</a>";
            $output .= "</li>";
      
        } 
        
        echo $output;
        echo "</div>";
    }
	
    public function form( $instance ) {
	
    }

    public function update( $new_instance, $old_instance ) {

    }
	
}
wp_enqueue_style( 
	'lexicon-css', 
	plugins_url('/css/style.css', __FILE__),
	false
);

wp_enqueue_script( 
	'lexicon-js', 
	plugins_url('/js/script.js', __FILE__),
	false
);

add_action( 'widgets_init', function(){
    register_widget( 'My_Widget' );
});

?>